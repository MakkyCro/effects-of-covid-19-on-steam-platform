from bs4 import BeautifulSoup
import requests
import csv

imena = []
linkovi = []

url = 'https://steamcharts.com/top'
page = requests.get(url)

soup = BeautifulSoup(page.content, 'html.parser')
tables = soup.findChildren('table')
igre = tables[0].find("tbody").find_all("tr")

# Preuzimanje imena i linkova za dohvacanje statistike
for i in igre:
    ime = i.find_all("td")[1].find(
        "a").text.replace("\t", "").replace("\n", "").replace(":", "")

    link = i.find_all("td")[1].find("a", href=True)

    imena.append(ime)
    linkovi.append("https://steamcharts.com" + link['href'])

    print(ime + " - " + link['href'])


# Prolazak kroz sve linkove, dohvaćanje naslova tablica, podataka u njima i spremanje u .csv datoteku odgovarajućeg naziva
header = []
redak = []
counter = 0

for num, link in enumerate(linkovi, start=0):
    url = link
    page = requests.get(url)

    soup = BeautifulSoup(page.content, 'html.parser')
    th = soup.find_all("th")
    tr = soup.find_all("tr")

    with open(imena[num]+'.csv', 'w', newline='') as file:
        writer = csv.writer(file)

        for naslov in th:
            header.append(naslov.text.replace("\n", ""))
        # print(header)
        writer.writerow(header)

        for r in tr:
            tds = r.find_all("td")
            for td in tds:
                redak.append(td.text.replace("\n", "").replace("\t", ""))
            counter += 1
            # print(redak)
            writer.writerow(redak)
            redak = []
    print("Spremljeno -", imena[num], "-",
          link, "\nZapisano:", counter-1, "retka.\n\n")
    header = []
    redak = []
    counter = 0
